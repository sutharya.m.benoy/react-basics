import React, {Component} from 'react';
import './App.css';
import User from './components/User';
import Data from './json/datalist';

class App extends Component {

    constructor() {
        super();
        this.state = {
            userList: Data,
            userindex: ""
        }
    }

    deleteUser = (index, e) => {
        const userNewList = Object.assign([], this.state.userList);
        userNewList.splice(index, 1);
        this.setState({userList: userNewList});

    }

    showDetails = (index, e) => {
        this.setState({userindex: index});
    }

    render() {
        let obj = null;

        if (this.state.userindex !== "") {
            let target = this.state.userList[this.state.userindex];
            obj = Object.keys(target).map((el, index) => {
                return (
                    <div key={el + index}>
                        <div>{el} : {target[el]}</div>
                    </div>
                )
            })
        }
        return (
            <div className="App"><h1>User List</h1>
                <ul>
                    {
                        this.state.userList.map((user, index) => {
                            return (<User
                                    name={user.name} age={user.age} key={user._id}
                                    deleteThis={this.deleteUser.bind(this, index)}
                                    showDetails={this.showDetails.bind(this, index)}
                                ></User>
                            );
                        })
                    }
                </ul>
                {obj}
            </div>
        );
    }
}

export default App;
