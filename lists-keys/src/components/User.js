import React from 'react';

const User = (props) =>{
  return(
      <li>
        <span> Name: {props.name}, Age: {props.age} </span>
        <button onClick = {props.deleteThis}>Delete</button>
       <span> <button onClick = {props.showDetails}>Show Details</button> </span>
      </li>
  )
}

export default User;