import React, {Component} from 'react';
import axios from 'axios';
import Table from '../Table';

class User extends Component {

    constructor() {
        super();
        this.state = {
            userList: [],
            currentPage: [],
            direction: {
                name: 'desc',
                username: 'desc',
                email: 'desc',
                phone: 'desc',
                website: 'desc',
            },
            startIndex: 0,
            endIndex: 5,
            pageNum:1,
            dataLen:0,
        }

        this.sortBy = this.sortBy.bind(this);
        this.nextPage = this.nextPage.bind(this);
    }

    sortBy(key) {
        let data = this.state.currentPage;
        let subset = data;
        if(this.state.startIndex === 0){
            subset = data.slice(this.state.startIndex, this.state.endIndex)
        }
        this.setState({
            currentPage: subset.sort((a, b) => (
                this.state.direction[key] === 'asc' ? a[key] > b[key] : a[key] < b[key])
            ),
            direction: {
                [key]: this.state.direction[key] === 'asc' ? 'desc' : 'asc'
            }
        })
    }

    nextPage() {
        let data = this.state.userList;
        let length = data.length;
        if (this.state.endIndex >= length) {
            this.setState({
                startIndex: 0,
                endIndex: 5,
                currentPage: this.state.userList.slice(0, 5),
                pageNum:1,
                direction: {
                    name: 'desc',
                    username: 'desc',
                    email: 'desc',
                    phone: 'desc',
                    website: 'desc',
                },
            });
        } else {
            let subset = data.slice(this.state.startIndex + 5, this.state.endIndex + 5)
            this.setState({
                startIndex: this.state.startIndex + 5,
                endIndex: this.state.endIndex + 5,
                currentPage: subset,
                pageNum: this.state.pageNum + 1,
                direction: {
                    name: 'desc',
                    username: 'desc',
                    email: 'desc',
                    phone: 'desc',
                    website: 'desc',
                },
            });

        }
    }

    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/users').then(res => {
            let data = res.data;
            data.sort((a,b) => {
                return a.name > b.name;
            });
            this.setState({userList: data,
                currentPage:data,
                dataLen:data.length,
            })

        });
        }

    render() {
        return (
            <div className="App"><h1>User List</h1>
                <Table currentPage={this.state.currentPage}
                       sortBy={this.sortBy}
                       startIndex={this.state.startIndex}
                       endIndex={this.state.endIndex}
                       nextPage={this.nextPage}></Table>
                <button onClick={this.nextPage}>{this.state.endIndex >= this.state.dataLen ? <p> Prev </p> : <p>Next</p>}</button>
                <div>Page {this.state.pageNum}</div>
            </div>
        );
    }
}

export default User;