import React, {Component} from 'react';

import Table from '../Table';
import {connect} from 'react-redux';
import actionCreator from '../store/actions/actions';


class User extends Component {

    componentDidMount() {
        this.props.axiosCall();
    }

    render() {
        return (
            <div className="App"><h1>User List</h1>
                <Table currentPage={this.props.currentPage}
                       sortBy={(key)=>this.props.sortBy(key)}
                       startIndex={this.props.startIndex}
                       endIndex={this.props.endIndex}
                       nextPage={this.props.nextPage}></Table>
                <button onClick={this.props.nextPage}>{this.props.endIndex >= this.props.dataLen ? <p> Prev </p> : <p>Next</p>}</button>
                <div>Page {this.props.pageNum}</div>
            </div>
        );
    }


}

const mapStateToProps = (state) => {
    return {
        userList: state.userList,
        currentPage: state.currentPage,
        direction: {
            name: state.name,
            username: state.username,
            email: state.email,
            phone: state.phone,
            website: state.website,
        },
        startIndex: state.startIndex,
        endIndex: state.endIndex,
        pageNum:state.pageNum,
        dataLen:state.dataLen,
    }
}
const mapDispatchToProps = (dispatch) => ({

    sortBy: (key) => dispatch({type: 'sortBy',key:key}),
    nextPage: () => dispatch({type: 'nextPage'}),
    axiosCall: () => dispatch(actionCreator)

});

export default connect(mapStateToProps,mapDispatchToProps)(User);