import React from 'react';

export default function UserTable(props) {
    let data = props.currentPage;
    let subset = data;

    if(props.startIndex === 0){
        subset = data.slice(props.startIndex, props.endIndex)

    }
    return (
        <table>
            <thead>
            <tr>
                <th>
                    <button onClick={() => props.sortBy('name')}>Name</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('username')}>Username</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('email')}>Email</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('address.street')}>Address</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('phone')}>Phone</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('website')}>Website</button>
                </th>
                <th>
                    <button onClick={() => props.sortBy('company.name')}>Company</button>
                </th>

            </tr>
            </thead>
            <tbody>{
                subset.map(row => (
                    <tr key={row.id}>
                        <td>{row.name}</td>
                        <td>{row.username}</td>
                        <td>{row.email}</td>
                        <td>{row.address.street},{row.address.suite},{row.address.city},{row.address.zipcode}</td>
                        <td>{row.phone}</td>
                        <td>{row.website}</td>
                        <td>{row.company.name}</td>
                    </tr>
                ))
            }

            </tbody>

        </table>



    )
}