

const initialState = {
    userList: [],
    currentPage: [],
    direction: {
        name: 'desc',
        username: 'desc',
        email: 'desc',
        phone: 'desc',
        website: 'desc',
    },
    startIndex: 0,
    endIndex: 5,
    pageNum:1,
    dataLen:0,
}

const reducer = (state = initialState,action) =>{
    const newState = {...state};

    switch(action.type){
        case "receive_users":
            let data = action.payload;
            data.sort((a,b) => {
                return a.name > b.name;
            })
            return{
                ...state,
                userList: data,
                currentPage: data,
                dataLen: data.length,
            }

        case "sortBy":
            let newState1 = {...state};
            let key = action.key;
            let data2 = newState1.currentPage;
            let subset1 = data2;
            if(newState1.startIndex === 0){
                subset1 = data2.slice(newState1.startIndex, newState1.endIndex)
            }

            return {
                ...state,
                currentPage: subset1.sort((a, b) => (
                    state.direction[key] === 'asc' ? a[key] > b[key] : a[key] < b[key])
                ),
                direction: {
                    [key]: state.direction[key] === 'asc' ? 'desc' : 'asc'
                },

            }


        case "nextPage":
            let newState2 = {...state};
            let data3 = newState2.userList;
            let length = data3.length;
            if (newState2.endIndex >= length) {
                return {
                    ...state,
                    startIndex: 0,
                    endIndex: 5,
                    currentPage: state.userList.slice(0, 5),
                    pageNum: 1,
                    direction: {
                        name: 'desc',
                        username: 'desc',
                        email: 'desc',
                        phone: 'desc',
                        website: 'desc',
                    },

                }
            } else {
                let subset = data3.slice(newState2.startIndex + 5, newState2.endIndex + 5)
                return {
                    ...state,
                    startIndex: state.startIndex + 5,
                    endIndex: state.endIndex + 5,
                    currentPage: subset,
                    pageNum: state.pageNum + 1,
                    direction: {
                        name: 'desc',
                        username: 'desc',
                        email: 'desc',
                        phone: 'desc',
                        website: 'desc',
                    },

                }
            }

    }

    return newState;

}

export default reducer;