import axios from 'axios';

const fetchUsers = () => {
    const request = axios.get('https://jsonplaceholder.typicode.com/users');

    return(dispatch) => {
        request.then(({data}) => {
            dispatch({type:"receive_users",payload:data})

        });
    }
}

export default fetchUsers();